---
title: Always be planning
layout: default
categories: organization, planning, engineering management
published: 2021-09-25
---

One (very biased) advice I give engineering managers is to always be planning. It means a variety of things, and folks usually take what they believe is most valuable for them. Let’s break it down.

## Ain’t there no one else who’s planning?

Usually planning doesn’t happen in isolation. Part of the management job is to make sure the entire organization works in a way that streams up to one big goal that, if achieved, would improve the chance of success for the entire company.  So unless you’re the only engineering manager in the org (which I wouldn’t recommend; always hire two), there’s a bunch of stuff you need to do.

The most commonly present counterpart in the planning is the product manager on your team. If you don’t have prior working relationship with your PM, it probably will take some time until you truly know and respect each other’s opinions, but before that, there’s always some planning to do.

Ultimately, the PM is responsible for execution on the product strategy in a way that fits the team’s mission, which is going to make up most of the team’s working time. Just be careful with putting undue emphasis on product: if there are technical challenges that can slow down or sink the team, don’t leave them unaddressed.

People criticize short-termism in line product management, but I actually think it’s okay. “No one knows the future anyway” is a pretty lame excuse to limit the horizon at three-month mark, but hey, that statement is correct in a very important way: it abstracts the big goal away from the work that’s going to land on the team, making it much easier for people who’re actually going to do the work to understand what needs to happen and to not get overwhelmed by how big the ambition might be.

As an engineering manager, you work with the plans on the product side of things and staff the team in a way that would make sense for the org and make it very likely that the product roadmap is going to get done, gradually, and that’s going to be professionally fulfilling for the team members. Here, thinking in quarters doesn’t really work (unless you’re part of a project org, with “resource pools” and shit), — you need to think much, much ahead of time, to make sure that, yes, the product goals are important, but also that people have clear career prospects.

I’ve heard that engineering management is art, and to me this sounds like another excuse to just wing it. But this puts you in a vulnerable position of being someone who, instead of building a system of decision-making, has to manage individual people’s disinterest and disengagement issues. (if you’re in a position where you have to put off fires like that all the time, I’m really, really sorry for you, but you gotta do something with it)

Your peers, other engineering managers, are also interested in the plan, especially when it comes to staffing, which informs hiring, which informs the “whose open spot is of higher importance” debate. That this debate is happening is a sign of a healthy engineering management team, because the hiring will almost always be behind the problems that need solving. The more you know about the parameters (product strategy, target engineering org structure, etc.), the better your staffing plan is going to be and the better you’ll understand which spots on the team are hot and unfulfilled, and which spots can wait while other teams take over the opportunity to build up.

From my experience, laying things out visually helps not repeat yourself, but the experience of people traversing and reading the materials can be anywhere on spectrum between bland and overwhelming, especially if they weren’t present 100% of the time you were working on them. After having been through this a couple times, it seems to me there’s no “sweet spot”, in the best case there’s an average of people’s preferences (see above: bland), so a better solution is putting lots of time into synchronous discussion. That’s exactly the type of live meeting that you shouldn’t cut on.

## What is the planning about, again?

No one really enjoys planning. Even for middle managers, they won’t come back home after 10 hours of work and say, “Honey, I’ve had a great planning today, I feel energetic and refreshed!” The first part of the sentence — yeah, maybe. But talking to a wide range of folks, I’ve seen all of them without exception rating planning as something that drains energy.

That’s not the point though. Gotta be planning anyway. But what exactly?

First of all, you want to make sure you’re in control of how your team or teams are evolving in terms of distribution of roles, jobs, skills, and also in terms of the number of people doing the job. Hiring really is fun, but running a team of 11 is not.

Sometimes the product goals are so ambitious, it looks like the team need even more than eleven people working on them. You start thinking, it’s a nice puzzle to crack. But hey, what are you doing just now? Yup, you had just been planning.

So the planning is an exercise that solves two problems: you understanding what actions to take and other people understanding why you took those actions. If you want to play on hard mode and decentralize the decision-making (which I personally totally recommend, it’s worth every gray hair), it also means that other people on the team will more likely take actions very similar to those you’d take.

## But why always?

The advice isn’t just “be planning”, it’s “always be planning”. And there’s a reason to that.

As an engineering manager, you have probably been exposed to a range of problems that the org needs solved, some of them technical, some more on the people side. On top of that, if you have some low-frequency rituals established in the org (quarterly+ planning, big architecture reviews, job performance reviews, etc.), they tend to, you know, happen exactly at the time they’re supposed to happen, which, if you’re already busy putting a fire out, adds to your workload, and you’re really, really not elastic, and yes, that 47-hour work week that you claimed to be 60-hour one is not healthy, whether you’re honest about it or not.

Some coping strategies exist, like shedding the load on the sides of the business that aren’t urgent right at the moment. Like, you don’t take on a few interviews this or that week, or you don’t show up on an indefinitely recurring meeting that has unpredictable effect on your energy level and half of the key participants dropped out already anyway. You can do that in a productive way or in a jerky, “leave me alone” way, but the reality is, the work postponed is the work that’s going to come at you with more force only slightly later.

How do you solve the problem of not being elastic? By balancing this work! And I’m really talking about an entire year. If you’re able to balance a whole year in a way that leaves you enough time which you’ll need to deal with immediate issues, this improves your sanity level and ability to comprehend information so, so much. You’re able to stay focused because you know that you’re at most times ahead of the schedule, because you’re prepared. It’s a good feeling.

## Okay, how?

A good strategy I found is to never delete any piece of information. In combination with a good culture of accumulating this information, you’re going to stop having a problem of looking some information up last minute and finding out it doesn’t exist. You’ll start having a problem of not being able to systematize existing information if it’s accumulated by different people with different preferences and quality standards.

There’s a ton of fantastic tools that allow for that. My faves so far are Notion for long-form writing people and Miro for visual thinkers, both highly collaborative and versatile.

Another way of spreading the planning over longer periods of time to load-balance is to make time for that explicitly. And by that I mean not a one-hour block in the morning, but sufficient time for research, thinking, and documenting all the things. When there’s a risk that people might misinterpret a blunt “I need to work on something, therefore I’m unavailable”, the managers’ best kept secret is faking a day off. Don’t play surprise, you’ve done this, too, even if inadvertently.

Finally, talking to someone about the things you’ve done (not the things you’re intending to do!) so far to ensure smooth operations works really well as a reinforcement mechanism. Many managers stay in the profession because (despite?) they are introspective and eager to improve, which means that even if that person you’re talking to us clueless, you’re still going to benefit from serializing your thoughts into a stream and hearing yourself say it.

---

The always-be-planning mindset is not easy to develop, not easy to retain. There will always be forces that will over time pull you into the local maxima of what everyone else is doing, and you might simply not be as good at that.  In the end, the always-be-planning strategy might not even work in your circumstances or for you personally. But it’s worth giving it a try, because, who knows, maybe it’ll save you not only clear mind and continued ability to focus, — who wouldn’t want that!
