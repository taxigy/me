---
title: Knowing vs. doing
layout: default
categories: short, abstract, thinking, false-dichotomy
published: 2021-04-13
---

One of the hard pills to swallow for me recently was (probably a false dichotomy, but still) a curious separation between knowing something and doing something. I noticed that each individual person is at some times focused on knowing things, which comes out as preaching a certain topic or presenting non-obvious arguments that no one would have thought of, and at some other times focused on doing things, such as writing a piece of code that adds unexpected value to a whole or recording a short explainer video that hits the combo of education and entertainment.

It struck me hard after I found myself out of balance, signiﬁcantly more on the knowing side than I was willing to admit. Knowing without doing usually looks like talking the talk but not walking the walk. Working with the talented bunch helped a lot, but the turning point was when I observed these people, some knowers and some doers, argue over things.

A good, healthy debate generates a lot of conﬂicting ideas. What makes it difﬁcult isn’t that the ideas are conﬂicting, but that sometimes people look at exactly the same idea from different points of view, use different expressions, say different words, and ultimately disagree. Being aware that it’s not the smart vs. dumb problem, it’s rather “your language vs. my language” problem. And staying in balance between knowing things (being smart about what action to take) and doing things (taking the damn action) helps detect that, make a pause, and reconcile the opinions into something meaningful.

*An obligatory note to my 2031-self:* keep checking on a regular basis whether the immediate circle of people you spend time with is diverse enough, but not crazy diverse to the point where the points of view are so different that it becomes misleading. Being among people who might not agree with each other most of the time, but collectively succeed at finding balance and truth is good, stick to those people, you old fart.

---

*This is an experimental post that is close in spirit to good ol’ times of classic blogging. Would be fun if it sticks.*