---
title: A properly sized work item
layout: default
categories: project-admin, task-management
published: 2020-04-28
---

tl;dr:
- distinguish product work and engineering work, be honest and don't try to hide one behind the other
- start from the end and work your way backwards to see a clear path between how things are (no feature) and how we want them to be (feature released)
- have three levels the work is split on
  - epics: long-lasting, weeks to months
  - stories and engineering tasks: fit into a sprint, not much movement while in the sprint
  - todos: daily movement in sublinear proportion to the size of the team
- review the three levels separately, with different goals in mind
  - epics: “Are we building the right thing?”
  - stories and engineering tasks: “Are we able to consistently deliver value on schedule?”
  - todos: “Are we marching steadily or are we stuck?”

## There are many types of work and they all matter

A very typical pattern is when a team has a product backlog and the work items on the backlog only loosely follow the storyline. They might belong in one higher-order item, usually a project or an epic, but other than that, they don't follow any order. This is usually by design: as a team, we want to slice the work in a way that would eliminate the order. It also happens most often due to a hidden waterfall avoidance and a push to be “more agile”.

From the product perspective, it only matters when it's done, and if we're dealing with a case of new feature development, then a collection of work items usually can be partitioned in a way that only a fraction of it contributes to the first release — an MVP, — and other subsets of works items represent the subsequent releases. This is also done with the intention to be able to walk away from a tail of releases when the feature development is no longer a priority for the team. And usually some of the work items only make sense to be done strictly sequentially.

If we transpose the same backlog to look at it from the engineering point of view, we can distinguish way more dependencies. Certain kinds of work, represented as separate work items, depend on others. We can't describe the data schema before we have a database, but we can't have a database before we're done with the data architecture. Alerting thresholds and capacity planning are two things that can go in any order, either way confusing. API and UI are mutually locking. The dependency chain is way more prominent when we think about how exactly we're going to build something.

When the team is looking at the backlog from either perspective, supplementary work items get added: analyze traffic patterns and do some guesswork for initial capacity planning and setting up alerts, or describe an API contract to make sure API and UI can be developed in parallel. These work items may or may not match the definition of ready, and may or may not contain proper description, but they represent the foundational work that needs to be put into the project before any feature development can start.

If the decision power on the team is skewed towards the product, the team learns over time to push the foundational work down the backlog, or treat it on the same level with feature work. This leads to things like investment into service monitoring being assigned lower priority than first essential features. This also leads to the team giving different estimates to similar-sized work items because the first one would require some prep work. A conversation similar to “If we do A first, it's XL, but if we do B first, then A is only M” is typical in the planning meetings.

One way to balance is to apply critical thinking to a plan. If the team is about to commit to some scope, but it contains foundational work that cannot seemingly be separated, then the team is about to commit to two separate scopes.[^teamwork-omitted]

[^teamwork-omitted]: While having a team to be honest with themselves on whether a certain feature work does or does not require extra prep work is as hard as it is important, this topic is out of scope of this post. A good book on the topic though would be [“Leadership is Language” by L. David Marquet](https://www.goodreads.com/book/show/42774083-leadership-is-language).

Both product work and engineering work are equally important and need to be kept separate.

## How we get there is more important than what we do

A planning meeting where the team slices a big project into smaller and smaller pieces usually generates a valuable backlog of work items. Once the team commits to it, the work quickly starts resembling mechanical ticket movement: take the top of the backlog, get it done, repeat. Along the way, product priorities change, the backlog get shuffled a bit, and then a bit more, up to the point where each of the work items makes total sense but the sequence is no longer original and it's nearly impossible tell whether it would be better to address the backlog in its latest order.

This is where the team needs to control the tool. If the tool — Jira, for example, — allows linking work items to loosely describe the sequence of steps, this is one of the way to make it harder for the team to commit to something that cannot be done due to it being blocked by other work items.

I've seen only a couple people who would take backlog management that seriously. This is mostly because backlog management isn't the most rewarding task for the engineers and also due to pervasive notion that the backlog can only be managed by a single person on the team, or sometimes even outside the team. To a lesser degree, reluctance to put this effort is also due to low promotability of the task: a successful release is a way stronger card against a well-administered backlog.[^long-haul]

[^long-haul]: One remark here is that, as an engineer, it's way easier to optimize for immediate rewards and application of core skill, which is writing code. A side-effect of a well-administered backlog would be higher likelihood of a _sequence_ of successful releases, but since it's not entirely controlled by the engineers, I totally understand why this behavior persists in general.

As soon as the team has interlinked backlog in place, deciding on what to work on next can happen automatically. The sequence can literally be programmed — although it probably makes sense to apply critical thinking at every case of putting a new work item in progress and reevaluate the links.

One useful tool that might help establish the process of linking is [inversion](https://fs.blog/2013/10/inversion/), where the team begins with the end goal (product or feature) and works their way backward to the current state (usually foundation being partially in place), considering dependencies and order or execution at every step. A result of this exercise might look similar to a [fishbone diagram](https://en.wikipedia.org/wiki/Ishikawa_diagram) or a directed acyclic graph with most nodes having one outgoing edge and some nodes having high fan-in.

## Size the work items

Having an interlinked backlog is a desired, although not necessary, condition to have properly sized work items. The two definitely work better together because they eliminate the temptation to conceal certain work types under the guise of features and help commit to a scope that has a high chance of being complete by the end of the iteration without team velocity shenanigans. But being honest about work types and splitting them accordingly is essential here.

For the team, it's important that

- the team gets a sense of progress
- the work gets done and the impediments get recognized easily
- various parties in the project get status, preferably automatically collected
- should cross-team dependencies pop up, people can make sense of what their and the other teams are committed to

If the team runs in sprints, and there's a sprint board (you need a board), it needs a moderate amount of movement, usually in a sublinear proportion to the number of team members, on that board — and the items moving should be lowest-level ones. In a team of five engineers, 2–3 work items need to change positions at almost every check-in, also allowing for some outlier cases. In a team of 7 engineers, a healthy number of changes would be around 3–5 per check-in. This would help focus the conversation around impediments and not spend a lot of time reciting code pushed or changes made.[^yti]

[^yti]: By the way, when the team runs a status update, like daily scrum or similar, the way it happens usually signals the productivity level the team is currently at. Focus on YTIs would mean people are focused on their individual tasks, optimizing for personal productivity, whereas focus on what the team has achieved as a whole would signal stronger emphasis on collective goals and team achievement.

At the same time, the team should be committed to a couple higher-level work items that contain meaning in terms of the product or the feature. Those are typically called user stories or just stories. Each story contains a use case that, when released, adds user value to the product. A user story might sound like “as a user, I can filter the list of music bands” and contain a collection of lower-level work items like “provide and fetch music band data on and from API”, “collect usage data on music band filters” and such.

Some of work items of equal size to stories cannot be tied to user value — that's the engineering work, or foundational work. Some tools distinguish between them and stories and some do not. The most important point here is that it's the mid-level work item.

On a yet higher level, the team should oversee longer-term goals. The items there are usually called epics and the purpose of having them is to have a brief and somewhat accurate answer to the question “Are we there yet?” and also to assign priorities to projects or bigger product features.

Most backlog management tools present epics as less tangible than work items at lower levels, which is by design: an epic is intended to take anywhere from two sprints up to infinity. This helps a bit, although sometimes it might force the team into trying to fit themselves into the product, not the other way around.

- highest-level items
  - called “epic”
  - have a vague start, potentially no end, likely flexible scope, and very long update cycle (weeks to months)
- mid-level items
  - usually called “stories”, although it's confusing when it comes to work that is as big, equally important, but does not directly reflect in user value
  - belong in a sprint, expected to be done within one sprint
- lowest-level items:
  - look like simple todo items
  - included in a story and, transitively, in a sprint
  - moves very fast, from in progress to done within hours
  - lightweight acceptance workflow

Note that the size stays in proportion to the scope then, although the whole (epic) is bigger than sum of its parts (user stories).

## Step back: when to review which size

Lowest-level work items, the todos, move fast on purpose: their movement is key to signal to the team how healthy the sprint is. No movement — we're stuck. Steady movement — we can accurately approximate whether we're on track.

Mid-level items, stories and engineering tasks, are designed to fit into one sprint but not cause a lot of movement within it. A perfect time to review the team's achievement at this level would be a review meeting — sprint review. The sprint review meeting is optimized for done, there's no ambiguity, so it generates a very different feedback for the team that ties better to how the end user would potentially think.

Finally, highest-level items, the epics, are long-lasting but still need to be reviewed. Checking on progress at this level would require a different flight level where the team can oversee not only the effort put into development but also the outcomes of that, the effect on the product and the users. Doing this every 3–6 sprints would help stay in touch with reality and correct the course when necessary (almost always necessary).
