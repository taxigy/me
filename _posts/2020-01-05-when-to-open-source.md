---
title: When to open-source as an engineering organization
layout: default
categories: organization, open-source
published: 2020-01-05
updated: 2020-06-07
---

Engineers love open source for different reasons:

- open source helps connect with a broader online community of smart and passionate people,
- working at a company that open-sources something feels like being a part of the whole world, working in the open,
- open-source contributions, as a side-effect, create a recruiting channel — and those popular repos look awesome on the resume, too!

Things that engineers forget that open source also is:

- a commitment to the community, with all the community management duties, e.g., keeping track of issues, PRs, answering questions, and sometimes not so smart ones,
- as a side-effect of that, more communication internally: what happens in the out-company that impacts the projects, has to be communicated to the internal teams as well to avoid costly surprises,
- even if implicitly and partially, making the roadmap public, either involuntarily (aka leaking) or purposefully, which means it has to be public-friendly, which means it has to be taken care of,[^company-reputation]
- opening (usually parts of the platform layer) developed by the company to the outside world, with all the implications for security, keeping an eye on vulnerabilities, and such,
- a minor one, but: feeling down because nobody gives a single look to the project, which brings us to the technology marketing responsibilities that also become part of the job.

[^company-reputation]: People expect a net-positive response from the broader community, otherwise it would be irrational to push for open-sourcing anything. But the response would be just a single event, or it would be a sequence of events of one kind. We also need to think in higher-order ranges: we are going to maintain it over time — what series of events can we anticipate? Which of those are going to be positive or negative? Who is doing what in which case?

There's a fantastic article, [The benefits (and costs) of corporate open source](https://increment.com/open-source/the-benefits-and-costs-of-corporate-open-source/) by Sophie Alpert. It describes similar challenges, but from a large company's perspective. A lot of points are going to be true even for a smaller company, although the argument that open-sourcing something is a huge investment is likely going to be perceived differently by people in a 100+ company than it is in a 1k+ or 10k+ companies, even though the size of the project's audience is what really matters, not the size of the team behind it.

A team, to determine properly whether they can afford open-sourcing a project, should do a couple things to increase the chance of being lucky:

- task management: think about what jobs will open-sourcing something involve, assign them, and see if the team still has time left to do the actual work (easy),
- risk management: think about what could go wrong at any point in the next 12 months, considering a range of possible scenarios, from running a project that becomes successful (i.e., attracts attention of the audience, becomes a widely used lib, etc.) to shutting it down at the same day, from finding out at some point that one of the forks becomes more popular to getting pwned due to a vulnerability that went unnoticed for some time while the project was used in one of the products of the company (hard),
- prepare for publishing: clean up the git history, [remove secrets](https://github.com/search?q=remove+passwords&type=Commits), add docs[^add-docs] (hard),
- and then retrofit the new, public version into other company's projects, the teams' dev tooling and build pipelines, and such (hard).

[^add-docs]: An open-source project rarely starts with great documentation — if any whatsoever. It's okay because the core value is in the code, not in the docs. Nevertheless, decently made documentation is very important for project adoption. Which, of course, requires skills and effort, which the team may not have at that moment.

Ideally, it should really be the team of engineers who own the project and decide, through a robust decision-making, to open-source it. If it's driven, risk-managed and decided by engineers, and it happens in the org consistently, it's a win. What likely wouldn't work is if there's not a single team behind the project, but rather a group of individuals from many teams (think workgroup or guild model): their direct teams will always remain a top priority, which will gradually reduce their motivation to maintain the open-source project.

In a healthy org, the path is usually clear as long as the team members are honest with each other about the motivation for open-sourcing something and about how things will change once a project that was private becomes public, and whether there are or aren't any other unproductive factors at play.
