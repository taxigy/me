---
title: "Bet: commoditized design systems ≠ white-label design systems"
layout: default
categories: design-systems
published: 2021-04-25
updated: 2021-05-02
---

Whenever I think about what design systems might become in future, a picture I imagine first is this: someone from a company goes online, googles a bit, and picks one of many white-label tools. They plug it it, and it just works, preferably in the cloud and at near-zero cost of integration. Just swipe the card, plug it into your design and developer tools, — and you got everything you need.

But this future doesn’t look satisfactory, nor is it likely to be true. If it were so simple, there would have been tremendous success stories already among the white-label design systems. The market would be undergoing crazy saturation. Company executives would ask their industry peers to recommend a trusted vendor. The category would have crowned the king.[^category] And Twitter people probably would be hating on some of the vendors that got popular among people who make buying decisions and not so much around those who are bound to actually use it.

[^category]: ref: [Play Bigger](https://www.goodreads.com/book/show/27064401-play-bigger)

What I believe to be a direct outcome of commoditization through automation for design systems is some sort of a tool that would only allow a change in the product if it “fits”. Each company would still work on a design system, including all the intra-organizational activities that constitute the bigger part of the process, but the conversation would likely change towards solutions.

If we’re talking about a system, there can be changes that fit perfectly, others that fit imperfectly, and it’s up to the tolerance threshold of a system to accept or reject such changes, and self-check its integrity before committing every change. If a design system allows a change in one of its components (and through that, its parameters), it better make sure that such a change does not cause negative side-effects in its dependencies, — way beyond the first-order ones.

So, a gate function.

Notice that there’s nothing about aligning priorities of design, product and engineering, or developing a visual grammar, or establishing a shared language, or making the look and feel of the product cohesive. If we’re talking about something that is commoditized and for the most part automated, by that time this is usually a non-issue.

There has been a lot of movement towards that, from atomic design and design tokens to server-driven UIs and design APIs. Just the fact that people are looking for a technical solution makes me think that at some point it will be found. Sure, people being smart and able to interact with each other about shared higher-order goals is going to stay important, but it’s likely going to transform from “let’s all agree it’s called ‘collapsible’, not ‘expandable’” to something like “let’s all rethink the parameters of the system; we can make it better for the user”.

So it’s all quite abstract, and that’s because it’s hard for me to imagine a clear future for a topic that is so “untechnical”. But it sure is gonna be something much more profound than the Sketch-to-Figma kind of step.