---
title: Knowledge accumulation and sharing
layout: default
categories: organization, management, knowledge
published: 2020-03-02
updated: 2020-08-30
---

tl;dr:

- document things that should be documented, so that new waves of hires can understand why things are the way they are and what's the direction
- for that, recognize what should be documented and distinguish that from things that can (and must) be automated or eliminated as waste in the first place
- for that, give the engineers — people who are ultimately responsible for doing that — time, tools and training to put important knowledge into docs
- for that, create incentives for engineers to document the knowledge and be enablers alongside operating as individual contributors — incorporate it into the career ladder, performance evaluation criteria, etc.

We're about 400 employees total and 100 employees in product or engineering roles, forming about a dozen teams, and in this setting it's really, really hard to accumulate knowledge and keep it uniformly distributed among, say, all engineers. Tradition of tribal knowledge, where people would simply talk to each other to exchange information, make decisions and seal them, worked just about 20 months ago, when the company was one-fifth of that, and teams of teams were only beginning to emerge.[^team-to-teams-to-teams-of-teams]

[^team-to-teams-to-teams-of-teams]: Des Traynor of Intercom has talked about “team → teams → teams of teams” evolution of a scaling organization in his talk “[Why people are key to scaling your startup](https://www.intercom.com/blog/videos/lessons-learned-from-scaling-a-team/)”. It's an interesting talk that uncovers a lot of complexity that is natural to scaling companies but hardly visible until it hits you in the back at full speed.

Since then, a lot of engineers have joined the company, right in the middle of a major rearchitecture effort. Now we have:

- a shrinking, in proportion to the rest of the org, “old guard”, a bunch of engineers who have been in the company for well over 2 years and who know how the software worked, and have a reasonably good grasp on how it works now
- the “new guard“: all the rest — a lot of engineers! — who joined the company in the last 20 months

The old guard still tends to operate in tribal mode. Quite literally, the engineers who were incredibly successful when the company was 40 people, by default choose to work the same way even though the company is now 400 people. Good: these engineers are aligned so well, they don't need to argue or even talk much, they just _do._ Bad: engineers of the new guard cannot keep up. You probably have had this experience: you join a team, it's so gelled you're feeling lucky, but then people start talking, and you see how they make decisions in minutes, and nothing of what they are talking about makes sense to you.[^old-guard]

[^old-guard]: Michael Lopp wrote about a classic clash between “old guard and “new guard in “[The Old Guard](https://randsinrepose.com/archives/the-old-guard/)”.

In an ideal world, when an engineer discovers knowledge, they broadcast it so that the others pick it up, and the others then actually pick it up. In reality, it's a lot messier: people don't have time, they don't see value in sharing something, or they overshare, or they don't follow, or they focus on their own stuff. The whole thing then drifts into misalignment. So it needs a dedicated effort to keep it afloat.

How though?

Do two things:

- train the pattern matching
- document in the open

Pattern matching is hard and requires all engineers to constantly learn to see what piece of information is worth putting on a record. It cannot be done with policies (which are, by the way, an instrument to make pattern matching easier), and for sure there's no Udemy course or anything like that which would teach how to see patterns in inherently noisy stream of events.

The biggest knowledge accumulation challenge of all times at scaling companies is whether a certain bit of information represents something valuable or not, and _what is its “best before” date._ Consider two dimensions:

- locally useful vs. globally useful
- useful in place vs. useful for generations to come

In the beginning, you'll see a lot of false positives: stuff residing in your knowledge base system of choice that has limited use, either in terms of reach or in terms of time. Evaluating every bit of knowledge on these dimensions is not a trivial task. It takes effort and skills. The more the org supports that through feedback and opportunities, the more likely it is to happen. In orgs where engineers are expected to only “deliver”, no documenting will take place, given that all engineers are hyperrational towards their incentives. In orgs where teamwork and enablement are valued as much as individual contribution, engineers will systematically optimize for doc skills.[^takes-time-be-patient-and-engaged]

[^takes-time-be-patient-and-engaged]: A good read on how this kind of systems works is Donella Meadows's “[Thinking in Systems: A Primer](https://www.goodreads.com/book/show/3828902-thinking-in-systems)”. Two takeaways from the book: 1) a change won't happen overnight, unless the feedback cycle is really, really tight (usually it isn't), and 2) actors in the system matter less than the actual system, so it's on the management to create incentives for engineers to optimize for teamwork and enablement of their peers and to tune down individual contribution a bit.

For having people train their pattern matching skills, a heuristic that I've seen most success with: welcome the false positives, but ask authors to categorize the information. When docs don't fit together, it's time to reevaluate what is worth keeping and what can be adjusted, merged, or deleted.

After that, documenting is easy, but writing in general doesn't come easy to a lot of people.[^writing-for-engineers-tbd]

[^writing-for-engineers-tbd]: I'm yet to discover a solid source of actionable advice for engineers on developing writing skills. Maybe one day at Egghead?

Besides, documenting skills are only rudimentary in most of the engineers for a reason: in the early days of a company, knowledge travels fast because everyone is in one room (alternatively, there's one channel on Slack). In most established companies, knowledge travels slower, but there's not a lot of it being generated on a daily basis. So it's mostly about companies that change very fast — by _scaling_ very fast, mostly. Not a lot of engineers, globally, have an opportunity in their careers to work in a company that scales or positively transforms rapidly.

We have a career ladder for engineers at Personio. Most of the criteria hint at enablement of peers; some of them are concrete and point at running the knowledge exchange at scale. This creates reasonable guidelines for engineers to avoid falling into the works-for-me local optimum. At this stage of the evolution, it makes a lot of sense to incentivize this for all engineers equally.

In the end, a few practical notes:

- create templates and examples for engineers to follow — it's _way_ easier for people to feel confident in what they're doing when they can follow an example or process
- at the most basic level, keep track of technical decisions: whenever a discussion takes place where engineers discuss a problem and many potential solutions (diverge), evaluate them against some criteria, and commit to one (converge), the process and the outcome need to be documented — practice makes perfect
- keep all the docs in one tool, preferably one that is easy to quickly pull up, search and navigate for an audience of engineers of various levels of maturity (so, probably not sources)
- encourage comments in the code, but keep an eye on waste
- make it a habit among engineers to [track technical debt]({% post_url 2019-09-29-acknowledge-technical-debt %}) and hold each other accountable
- use the regular meetings to broadcast knowledge: invite people from the broader org to sprint review (in case you run Scrum) or team office hours (if you don't)
- keep an eye on knowledge fragmentation among teams and cohorts — only visible as a pattern, hard to tell from just one occasion
