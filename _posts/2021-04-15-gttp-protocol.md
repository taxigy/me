---
title: "GTTP(S) protocol: get to the point (soon)"
layout: default
categories: communication, persuasion
published: 2021-04-15
---

When in a meeting with a group of people, and the goal is rapid decision-making and figuring out a plan, I often find it hard to make a point. People are usually very different, they have different preferences, perceive verbal stream of ideas through the selective sieve of their own unique experience. In a way, each one is at risk of selective listening, so I often fall back into trying to tailor for everyone. This makes me going into “story mode”, talking for minutes and minutes, trying to explain just a handful of simple things. The irony then is, the longer the message, the lower the chance of retention. People listen to the first couple seconds and then mentally sign off.

What I noticed among the best communicators is, they optimize for having the majority, not all attendees, to get what they’re trying to say. They get to the point extremely fast, within the first 3–4 seconds of their speech. The absolute zen of getting to the point is when this sounds emotionally loaded but neither offensive nor dismissive.

It’s okay to be a slow talker, as long as you get to the point as soon as possible. How? By doing a few things:

- get everyone prepared prior to the discussion, — deal with lack of preparedness decisively
- build upon the ongoing discussion, don’t break the storyline, — stop every digression quickly and pull the discussion back to its original route
- and propose terminating the discussion if it’s not approximating a meaningful conclusion in a unit of time, — pick the tolerance threshold based on group’s cognitive ability, with razor thin buffer

This only works for groups where people can, theoretically, achieve a state of agreement. That’s not every group, but you really want to be part of such group.
