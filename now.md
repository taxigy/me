---
title: /now - Rishat
layout: default
---

Things I'm doing now (approximately):

- Homeschooling and babysitting, est. 2020
- Learning basic throat singing (khoomei) and inward bass, — making weird sounds really is fun
- Working from home at [Personio](https://www.personio.de/) as part of the design system and UI platform team

Last updated in June 2021.
